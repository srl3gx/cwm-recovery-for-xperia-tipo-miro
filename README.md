- - -

# CWM Recovery For Xperia Tipo/Miro/J


- - -

**Initial Setup**

- - -

First download the cyanogenmod source code and download all tools required for compiling the source code. You will find a lot of posts in XDA on how to do that. Then use the commands below.

    cd CyanogenModRootDirectory/bootable
    rm -irf recovery
    git clone https://bitbucket.org/srl3gx/cwm-recovery-for-xperia-tipo-miro.git recovery
    cd ..
    make -j2 otatools

- - -

**For compiling CWM for Xperia Tipo**

- - -

    bootable/recovery/setup.sh tipo
    recoverybuild.sh tipo

The compiled recovery will be stored in `offline_recovery/tipo`. For installing CWM go to `offline_recovery/tipo` and use terminal to run `install.sh`

- - -

**For compiling CWM for Xperia Miro**

- - -

    bootable/recovery/setup.sh miro
    recoverybuild.sh miro

The compiled recovery will be stored in `offline_recovery/miro`. For installing CWM go to  `offline_recovery/miro` and use terminal to run `install.sh`

- - -

**For compiling CWM for Xperia J**

- - -

    bootable/recovery/setup.sh j
    recoverybuild.sh j

The compiled recovery will be stored in `offline_recovery/j`. For installing CWM go to  `offline_recovery/j` and use terminal to run `install.sh`
