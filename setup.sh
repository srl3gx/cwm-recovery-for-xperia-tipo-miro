#!/bin/bash

function usage
{
    echo Usage:
    echo "  setup.sh tipo for setting tipo"
    echo "  setup.sh miro for setting miro"
    exit 0       
}

echo 
echo 
echo "*********************************************"
echo "         CWM for Xperia Tipo/Miro/J            "
echo "*********************************************"
echo 
echo 

MODEL=$1

CWMDIR=$PWD/bootable/recovery
FILESDIR=$CWMDIR/files
BUILDDIR=$FILESDIR/recoverybuild.sh

FILES=$FILESDIR/$1

DEVICE=""

if [ $MODEL == "tipo" ]; then
	DEVICE="ST21i"

elif [ $MODEL == "miro" ]; then
	DEVICE="ST23i"

elif [ $MODEL == "j" ]; then
	DEVICE="ST26i"

else
	echo "Error:Unknown device $1"
	exit 0
	
fi

echo "Setting up cwm for $1"
echo $FILES
$PWD/build/tools/device/mkvendor.sh Sony $DEVICE $FILES/boot.img
echo
echo
echo
echo "Copying configuration files....."
cp $FILES/conf/* device/Sony/$DEVICE
echo "Done"
echo
echo "Now run recoverybuild.sh $1"

cp $BUILDDIR .
chmod +x recoverybuild.sh

